import pytest
from time import sleep

from page_model import MainPage, TranslationPage

test_text = "Jak co roku obchody lanego poniedziałku w Bełchatowie wywołują wiele emocji. Święto to jest, co prawda, uświęcone tradycją, jednakże są ludzie, którzy zdecydowanie przesadzają z hołdowaniem tej jednej tradycji. \"Świństwo, kiedyś to dziewczyny się perfumami lało, a nie tak, jak teraz, wiadrami; tałatajstwo!!!\", oburza się starszy pan, zapytany o opinię na temat nadchodzącego poniedziałku. W ubiegłym roku śmigus - dyngus okazał się wyjątkowo mokry i wyjątkowo brzemienny w skutkach. W wyniku \"niewinnych\" igraszek z wodą doszło do wypadku samochodowego, w wyniku którego jedna osoba doznała poważnych obrażeń kręgosłupa. Było to na szczęście jedyne tak tragiczne wydarzenie owego dnia. Wczoraj o godzinie osiemnastej odbyło się zebranie Stowarzyszenia Bełchatowskich Rodzin, na którym omawiano drażniący problem lanego poniedziałku. Wysunięto także propozycję, aby na ulicę wyszły prewencyjnie liczniejsze niż zwykle patrole policji i straży miejskiej. Rodziny zaapelowały do młodzieży, żeby ta nie przesadzała z wodą. Tymczasem sama młodzież mówi, że nic złego się nigdy nie dzieje. \"My się oblewamy między sobą, serio. Na pewno do żadnego dorosłego kolesia z butlami nie przylecimy. Nie wiem, czemu wszyscy tak się tego poniedziałku boją!\". Koledzy mojego rozmówcy tylko przytakują głowami. Nie ulega wątpliwości, że temat lanego poniedziałku wywołał liczne komentarze i wątpliwości, zarówno po stronie amatorów tego tradycyjnego polskiego obrzędu, jak i po stronie bełchatowian obawiających się o swoje bezpieczeństwo. Obecnie można tylko czekać na decyzję kompetentnych organów i poważnie się zastanowić nad problemem śmigusa - dyngusa. Ludzie twardo stąpający po ziemi nie chcą wierzyć, że przed półwieczem wydarzyło się dla Ziemi coś tak niezwykłego. Otóż pół wieku temu człowiek postawił nogę na Księżycu. Kiedy wieczorem spoglądamy na rozgwieżdżone niebo z sierpem księżyca, trudno wręcz przypuszczać, że można po nim chodzić. W naszej wyobraźni tylko legendarny Mistrz Twardowski, zwany polskim Faustem, wylądował tam na kogucie I już pozostał. Poświęcono mu bez mała trzydzieści utworów, filmów, performance'ów, a nawet grę komputerową. Na Księżyc latali bohaterowie książek I filmów science fiction. O próbie kolonizacji ziemskiego satelity traktuje trylogia Jerzego Żuławskiego \"Na srebrnym globie\" sprzed ponad stu lat. Po więcej niż półwieczu jego stryjeczny wnuk, Andrzej Żuławski nakręcił film pod tym samym tytułem. A niespełna dwadzieścia lat wcześniej wielki krok dla ludzkości zrobił Neil Armstrong, stawiając stopę na Księżycu."

@pytest.mark.usefixtures("driver")
def test_translation(driver):
    main_page = MainPage(driver=driver)
    main_page.navigateToTranslationPage()

    translation_page = TranslationPage(driver=driver)
    translation_page.textboxWithTextToTranslate.send_keys(test_text)
    translation_page.languageSelectionDropdown.click()

    # English is preselected as default language and we need to deselect it
    translation_page.clickLanguage("english")
    translation_page.clickLanguage("deutsch")

    translation_page.proofreadingCheckbox.click()

    sleep(5)

    assert translation_page.getTimeToFinish() is not None
    assert translation_page.getTranslationCost() is not None
