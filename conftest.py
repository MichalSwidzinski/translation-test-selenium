import pytest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service as ChromiumService

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.core.utils import ChromeType

@pytest.fixture()
def driver(request):
    options = Options()
    options.add_argument('--single-process')
    options.add_argument("--remote-debugging-port=9222")

    driver = webdriver.Chrome(service=ChromiumService(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install()), options=options)
    driver.get("https://turbotlumaczenia.pl/")
    driver.maximize_window()

    yield driver

    driver.quit()
