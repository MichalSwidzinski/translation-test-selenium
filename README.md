# Selenium Test Translation

### Dependencies
- Python 3.9
- Additional requirements in requirements.txt

### Setup
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python3 -m pytest
```

### Authors
- Michał Świdziński