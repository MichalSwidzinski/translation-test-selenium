from selenium.webdriver.common.by import By

class MainPage():
    def __init__(self, driver):
        self.driver = driver

    def _get_element(self, css_selector):
        return self.driver.find_element(By.CSS_SELECTOR, css_selector)

    def navigateToTranslationPage(self):
        button = self._get_element("#clouds > div.col-sm-12 > div > a")
        button.click()


class TranslationPage():
    def __init__(self, driver):
        self.driver = driver
        self.languageSelectionDropdown = self._get_element("#dropdown-col-to1")
        self.textboxWithTextToTranslate = self._get_element("#content")
        self.proofreadingCheckbox = self._get_element("#proofreading")

    def _get_element(self, css_selector):
        return self.driver.find_element(By.CSS_SELECTOR, css_selector)

    def clickLanguage(self, language):
        languages = {
            "english":"#Row_1en",
            "deutsch":"#Row_16de"
        }
        button = self._get_element(languages[language])
        button.click()

    def getTimeToFinish(self):
        return self._get_element("#automatic-quote > div:nth-child(1) > div > div:nth-child(1) > span").text

    def getTranslationCost(self):
        return self._get_element("#automatic-quote > div:nth-child(2) > div > div:nth-child(1) > span").text
